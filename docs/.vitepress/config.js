export default {
	lang: 'en-US',
	title: 'KoteraHQ',
	description: 'Technical documentations for KoteraHQ products.',
	lastUpdated: true,
	markdown: {
		theme: 'dark-plus',
	},
	cleanUrls: 'with-subfolders',

	themeConfig: {
		logo: {
			light: '/logo.svg',
			dark: '/logo-white.svg',
		},
		socialLinks: [
			{ icon: 'discord', link: 'https://discord.gg/JSHRQkrafN'},
			{ 
				icon: {
					svg: '<svg xmlns="http://www.w3.org/2000/svg" width="192" height="192" fill="#000000" viewBox="0 0 256 256"><rect width="256" height="256" fill="none"></rect><path d="M238.3,152.5,211,50a12,12,0,0,0-22.9-1.1L167.5,104h-79L67.9,48.9A12,12,0,0,0,45,50L17.7,152.5a16.1,16.1,0,0,0,6.4,17.3l94.8,65.6a15.5,15.5,0,0,0,7.1,2.7h4a15.5,15.5,0,0,0,7.1-2.7l94.8-65.6A16.1,16.1,0,0,0,238.3,152.5ZM33.2,156.6,42.9,120H77.5l34,90.9Zm111.3,54.3,34-90.9h34.6l9.7,36.6Z"></path></svg>'
				},
				link: 'https://gitlab.com/koterahq'
			}
		],
		nav: [
			{
				text: 'Libraries',
				items: [
					{ text: 'Koute', link: '/koute/' },
				]
			}
		],
		sidebar: {
			'/koute/': [
				{
					text: 'Pinned',
					collapsible: true,
					items: [
						{ text: 'Homepage', link: '/koute/' },
						{ text: 'Migrating from 0.x', link: '/koute/migrate' },
					]
				},

				{
					text: 'Guide',
					collapsible: true,
					items: [
						{ text: 'Introduction', link: '/koute/guide/introduction' },
						{ text: 'Installation', link: '/koute/guide/installation' },
						{
							text: 'Fundamentals',
							items: [
								{ text: 'Foreword', link: '/koute/guide/fundamentals/' },
								{ text: 'Setting up Koute', link: '/koute/guide/fundamentals/setup' },
								{ text: 'Navigating through pages', link: '/koute/guide/fundamentals/navigation' },
								{ text: 'Rendering the result', link: '/koute/guide/fundamentals/render' },
								{ text: 'Using meta', link: '/koute/guide/fundamentals/meta' },
							]
						},
					]
				},

				{
					text: 'API reference',
					collapsible: true,
					items: [
						{ text: 'Foreword', link: '/koute/api/' },
						{ text: 'Router', link: '/koute/api/router' },
						{ text: 'Route', link: '/koute/api/route' },
						{ text: 'Meta', link: '/koute/api/meta' },
						{ text: 'Canvas', link: '/koute/api/canvas' },
					]
				},
			]


		},

		footer: {
			copyright: 'Copyright © 2022-present KoteraHQ',
		},
	},
}