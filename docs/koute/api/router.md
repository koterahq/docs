# Router
```lua
function Router(props: Types.RouterParams): Types.Router
```

Constructs and returns a new router object.

## Parameters
- `props: Types.RouterParams` - the properties for the router.
	- `[Fusion.Children]` - the table of routes for the router.

## Object Methods
### `go()`
```lua
function Router:go(path: string, props: { any }?)
```

Navigates to a new page with optional page parameters.

If the router is serving a page in the history and is now navigating to a new page, pages newer than the page being served will be overwritten.

### `back()`
```lua
function Router:back(level: number?)
```

Go back to the previous `n` page, where n is the value of the `level` parameter. `1` will be used if `level` is not provided.

### `set()`
```lua
function Router:set(route: Types.Route, params: { any }, direction: "go"|"back")
```

Sets the currently-served page to a new page. This object method should not be used as this is mainly designed for internal usage.

## Object Members
### `type`
```lua
Router.type: "Router"
```
The type of the object.

### `serving`
```lua
Router.serving: {
	path: Fusion.State<string>,
	view: Fusion.State<({ any }) -> (GuiObject)>,
	meta: Fusion.State<{ any }>,
	params: { any },
}
```

A table containing information regarding the currently-served page.

### `history`
```lua
Router.history: { Types.DeconstructedRoute }
```

A table holding records of previously-visited pages.

### `routes`
```lua
Router.routes: { [string]: Types.DeconstructedRoute }
```

A table holding all present and available routes.