# Foreword
The API reference is technical documentation for every public API exposed by the Koute module.

## Navigation
The public API are found in the sidebar on your left, named after the class name.

## Type information
On many pages in the API reference, you will see type annotations describing the API member. For example:
```lua
function Router(props: Types.RouterParams): Types.Router
```

While these type annotations are designed to be Luau-like, they are ultimately psuedocode included as a developer aid. For fully accurate and syntactically valid type information, please refer to the source code directly.