# Meta
```lua
function Meta(name: string): Symbol
```

Generates symbols used to denote meta value when working with the [Route](./route) function.

When using this symbol as a key in `Route`'s property table, a value is expected.

## Parameters
- `name: string` - the name of the meta symbol.

## Example usage
```lua
Route "/abc" {
	view = function()
		return New "TextLabel" {
			Text = "Hello, world!"
		}
	end,
	[Koute.Meta "abc"] = "def",
}
```