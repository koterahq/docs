# Route
```lua
function Route(path: string): (props: Types.RouteParams) -> (Types.Route)
```

Constructs and returns a new route object.

The function has curried parameters - when calling `Route` with the `path` parameter, it'll return a second function accepting the `props` parameter. This is done to take advantage of some function call syntax sugar in Lua:
```lua
Route("/abc")({ ... })
-- is equivalent to:
Route "/abc" { ... }
```

## Parameters
- `path: string` - the path string, an unique identifier for the route
- `props: Types.RouteParams` - the properties for the route.
	- `view: ({ any }?) -> (GuiObject)` - the function creating the page

## Example usage
```lua
Route "/abc" {
	view = function()
		return New "TextLabel" {
			Text = "Hello, world!"
		}
	end,
}
```