# Canvas
```lua
function Canvas(props: Types.CanvasParams): Frame
```

Constructs and returns a new canvas object.

## Parameters
- `props: Types.CanvasParams` - the properties for the canvas.
	- `source` - the render source for the canvas.
	- `preRender` - a lifecycle function, calls before a page gets rendered
	- `postRender` - a lifecycle function, the opposite of `prerender`