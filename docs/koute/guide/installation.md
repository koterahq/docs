# How to install Koute?
Multiple methods are offered for the installation of Koute. Choose one that suits you and your project best:

## Via Wally
Wally is a package manager for Rojo-managed projects. If your project is setup with Wally and Rojo, you may choose this method to install Koute. Append the line below to your project's `wally.toml`:

```toml
Koute = "koterahq/koute@1.0.3"
```

::: info
Bear in mind that this documentation may not be updated when a new update gets released, you should check [wally.run](https://wally.run/package/7kayoh/koute) for the latest version available.
:::

## Via Rojo
If your project is setup with Rojo but not Wally, you can install Koute by downloading the latest release's source code, and have it synced in your project's directory. Get the release from [this page](https://gitlab.com/koterahq/koute/prod/-/releases).

## Install manually
If your project is not setup with Wally and Rojo, you can install Koute manually. Get the latest `.rbxm` file from [this page](https://gitlab.com/koterahq/koute/prod/-/releases) and drag the downloaded file into Roblox Studio.