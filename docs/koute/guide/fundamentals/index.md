# Foreword
The fundamentals section is a series of guides teaching you how to learn the basic of Koute.

## What you need to know
These tutorials assume:
- You are comfortable with Roblox and the Luau scripting language.
- You are familiar with how UI works on Roblox.
- You are familiar with Fusion.
- You have installed Koute and Fusion via Wally.

Of course, based on your existing knowledge, you may find some of the tutorials easier or harder. Koute is built to be easy to learn, but it may still take a bit of time to absorb some concepts, so don't be discouraged. :smile:

## How these guides work
All the fundamentals guides can be found in the sidebar to your left. The guides are sorted in the order of difficulty, from beginner to advanced. We recommend you to start from the first and to last so you will be able to understand every part of Koute easily.

Every guide starts with the required code and ends with the finished code. When heading to the next guide, you should make sure your code is the same as the required code mentioned in the beginning of corresponding guide.