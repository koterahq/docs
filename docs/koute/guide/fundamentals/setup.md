# Setting up Koute
::: info Required code
This documentation assumes you installed Koute and Fusion via Wally...

```lua
local ReplicatedStorage = game:GetService("ReplicatedStorage")

local Packages = ReplicatedStorage.Packages

local Fusion = require(Packages.Fusion)
local Koute = require(Packages.Koute)
```
:::

Now we have installed Koute in our project, let's start by creating a router and a few routes.

## Creating a router
In order to integrate Koute into your project, you need to first create a router with the module. The required Koute module exposes constructor functions for Router, Route, Canvas and Meta. We'll teach you all of them in this documentation, but for now, we'll start with Router and Route first.

Koute's constructor functions are very alike how you create instances with `Fusion.New`, here's how:

```lua
local appRouter = Koute.Router {
	
}
```

::: tip
You might want to have the constructor functions to be assigned to variables just in case if the function gets renamed.
:::

:tada: A router is now created! What is left is create routes for the router so it will be fully functional.

## Creating routes
Router is created, but how do we actually navigate through pages? **By defining routes to the router.** As mentioned above, Koute exposes the constructor for route, so in order to define a route, we just have to call `Koute.Route`:

```lua
Koute.Route "/abc" {
	
}
```

Unlike how you create a router, `Koute.Route` accepts a string first, then a table of properties describing the route. The string argument is a path, the path is like an unique identifier of the route which is used for router navigation.

The table is for the properties of the route, such as `view`, the page constructor function, `[Koute.Meta]`, meta value, and `[Fusion.Children]` a list of children routes. The page constructor function should return a `GuiObject` when called. Here's an example:

```lua
Koute.Route "/abc" {
	view = 	function()
		return Fusion.New "TextLabel" {
			Size = UDim2.fromOffset(200, 50),
			Text = "Hello, world!"
		}
	end,
}
```

In the example above, we have created a route at `/abc` displaying a `TextLabel` with text `Hello, world!`.

___

Now, we have created a route, we need to put it into the router with `[Fusion.Children]`:
```lua
local appRouter = Koute.Router {
	[Fusion.Children] = {
		Koute.Route "/abc" {
			view = function()
				return Fusion.New "TextLabel" {
					Size = UDim2.fromOffset(200, 50),
					Text = "Hello, world!"
				}
			end,
		},
	},
}
```

### Children routes
For routes that starts with the same path, you can make the routes as children routes. Below is an example showing the usage of children routes with routes `/abc`, `/abc/def`, and `/abc/xyz`:

```lua
local function createView(text: string): () -> (TextLabel)
	return function()
		return Fusion.New "TextLabel" {
			Size = UDim2.fromOffset(200, 50),
			Text = text,
		}
	end
end

local appRouter = Koute.Router {
	[Fusion.Children] = {
		Koute.Route "/abc" {
			view = createView("at /abc"),
			[Fusion.Children] = {
				Koute.Route "/def" {
					view = createView("at /def"),
				},

				Koute.Route "/xyz" {
					view = createView("at /xyz"),
				},
			}
		}
	}
}
```

Children routes have infinite depth, you can create route like `/abc/def/ghi/jkl`. However, **bear in mind** that greater depth means longer wait for the route to be created.

___
::: info Finished code
```lua
local ReplicatedStorage = game:GetService("ReplicatedStorage")

local Packages = ReplicatedStorage.Packages

local Fusion = require(Packages.Fusion)
local Koute = require(Packages.Koute)

local function createView(text: string): () -> (TextLabel)
	return function()
		return Fusion.New "TextLabel" {
			Size = UDim2.fromOffset(200, 50),
			Text = text,
		}
	end
end

local appRouter = Koute.Router {
	[Fusion.Children] = {
		Koute.Route "/abc" {
			view = createView("at /abc"),
			[Fusion.Children] = {
				Koute.Route "/def" {
					view = createView("at /def"),
				},

				Koute.Route "/xyz" {
					view = createView("at /xyz"),
				},
			}
		}
	}
}
```
:::