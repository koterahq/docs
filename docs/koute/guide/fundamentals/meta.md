# Using meta
::: info Required code
```lua
local Players = game:GetService("Players")
local ReplicatedStorage = game:GetService("ReplicatedStorage")

local LocalPlayer = Players.LocalPlayer
local Packages = ReplicatedStorage.Packages

local Fusion = require(Packages.Fusion)
local Koute = require(Packages.Koute)

local function createView(text: string): () -> (TextLabel)
	return function()
		return Fusion.New "TextLabel" {
			Size = UDim2.fromOffset(200, 50),
			Text = text,
		}
	end
end

local appRouter = Koute.Router {
	[Fusion.Children] = {
		Koute.Route "/abc" {
			view = createView("at /abc"),
			[Fusion.Children] = {
				Koute.Route "/def" {
					view = createView("at /def"),
				},

				Koute.Route "/xyz" {
					view = createView("at /xyz"),
				},
			}
		}
	}
}

Fusion.New "ScreenGui" {
	Parent = LocalPlayer.PlayerGui,
	[Fusion.Children] = {
		Koute.Canvas {
			source = appRouter,
			postRender = function()
				print("Hello, world!")
			end,
		},
	},
}
```
:::

Meta is a type of value that changes over time and usually should be changed by routes only. Unlike parameters, the value of a meta is not determined by the user, but by the route instead.

Meta is useful for adding/reading additional metadata of a route. For instance, a title bar with its text dependent to the currently-served page.

To use a meta, call `Koute.Meta` with the meta name and assign it to the value. Just like how you declare an `Fusion.OnEvent`/`Fusion.OnChange`:

```lua
Koute.Route "/xyz" {
	view = createView("at /xyz"),
	[Koute.Meta "isPagePublic"] = true,
}
```

The meta can be found inside `appRouter.serving.meta`, a `Fusion.State` wrapped dictionary:

```lua
print(approuter.serving.meta:get().isPagePublic)
```

Here is an example showing the usage of meta:

```lua
...

local appRouter = Koute.Router {
	[Fusion.Children] = {
		Koute.Route "/abc" {
			view = createView("at /abc"),
			[Fusion.Children] = {
				Koute.Route "/def" {
					view = createView("at /def"),
				},

				Koute.Route "/xyz" {
					view = createView("at /xyz"),
					[Koute.Meta "isPagePublic"] = true,
				},
			}
		}
	}
}

Fusion.New "ScreenGui" {
	Parent = LocalPlayer.PlayerGui,
	[Fusion.Children] = {
		Koute.Canvas {
			source = appRouter,
			postRender = function()
				print("Is this page public? ", tostring(approuter.serving.meta:get().isPagePublic or false))
			end,
		},
	},
}
```

___
::: info Finished code
```lua
local Players = game:GetService("Players")
local ReplicatedStorage = game:GetService("ReplicatedStorage")

local LocalPlayer = Players.LocalPlayer
local Packages = ReplicatedStorage.Packages

local Fusion = require(Packages.Fusion)
local Koute = require(Packages.Koute)

local function createView(text: string): () -> (TextLabel)
	return function()
		return Fusion.New "TextLabel" {
			Size = UDim2.fromOffset(200, 50),
			Text = text,
		}
	end
end

local appRouter = Koute.Router {
	[Fusion.Children] = {
		Koute.Route "/abc" {
			view = createView("at /abc"),
			[Fusion.Children] = {
				Koute.Route "/def" {
					view = createView("at /def"),
				},

				Koute.Route "/xyz" {
					view = createView("at /xyz"),
					[Koute.Meta "isPagePublic"] = true,
				},
			}
		}
	}
}

Fusion.New "ScreenGui" {
	Parent = LocalPlayer.PlayerGui,
	[Fusion.Children] = {
		Koute.Canvas {
			source = appRouter,
			postRender = function()
				print("Is this page public? ", tostring(approuter.serving.meta:get().isPagePublic or false))
			end,
		},
	},
}
```
:::