# Navigating through pages
::: info Required code
```lua
local ReplicatedStorage = game:GetService("ReplicatedStorage")

local Packages = ReplicatedStorage.Packages

local Fusion = require(Packages.Fusion)
local Koute = require(Packages.Koute)

local function createView(text: string): () -> (TextLabel)
	return function()
		return Fusion.New "TextLabel" {
			Size = UDim2.fromOffset(200, 50),
			Text = text,
		}
	end
end

local appRouter = Koute.Router {
	[Fusion.Children] = {
		Koute.Route "/abc" {
			view = createView("at /abc"),
			[Fusion.Children] = {
				Koute.Route "/def" {
					view = createView("at /def"),
				},

				Koute.Route "/xyz" {
					view = createView("at /xyz"),
				},
			}
		}
	}
}
```
:::

We have successfully created a router and routes for the project. The next step is to do page navigation with the router, but how?

## Navigating to a new page
The created router (`appRouter`) exposes methods and members of the class. In this section, we will be using the method `:go()`.

The `:go()` method requires the path of the page and accepts optional parameters for the page. We will be also explaining what parameters are in this section.
```lua
appRouter:go("/abc/def")
```

The code above makes the router navigate to the page `/abc/def`.

### Page parameters
We have mentioned that the `:go()` method accepts a table of parameters for the page. To actually use the parameters in the route, you just have to assign the first and the second parameter of the `view` function to a variable. The first parameter is a reference to the router class (if you happened to create the router in another script), and a reference to the `serving` table found in the router class.

Here is a visualized diagram of the `serving` table:
- `path: Fusion.State<string>`
- `history: { Type.Routes }`
- `meta: Fusion.State<{ any }>`
- `params: { any }`

and here is an example showing the usage of page parameters:
```lua
local function createView(text: string): () -> (TextLabel)
	return function(router, serving)
		return Fusion.New "TextLabel" {
			Size = UDim2.fromOffset(200, 50),
			Text = text .. (serving.params.text or ""),
		}
	end
end

...

appRouter:go("/abc/def", { text = " hello koute!" })
```

The line calling `:go()` will make `appRouter` navigates to `/abc/def`, which returns a `TextLabel` with text `at /abc/def hello koute!`.

## Going back
A browser has the ability to go back and Koute has it too! Simply call `:back()` and the router will go back automatically. If you want to go back multiple levels, just provide the number of levels to go back to in the method function.

```lua
appRouter:back(2)
```

The code above will make `appRouter` goes back by 2 if possible.

___
::: info Finished code
```lua
local ReplicatedStorage = game:GetService("ReplicatedStorage")

local Packages = ReplicatedStorage.Packages

local Fusion = require(Packages.Fusion)
local Koute = require(Packages.Koute)

local function createView(text: string): () -> (TextLabel)
	return function(router, serving)
		return Fusion.New "TextLabel" {
			Size = UDim2.fromOffset(200, 50),
			Text = text .. (serving.params.text or ""),
		}
	end
end

local appRouter = Koute.Router {
	[Fusion.Children] = {
		Koute.Route "/abc" {
			view = createView("at /abc"),
			[Fusion.Children] = {
				Koute.Route "/def" {
					view = createView("at /def"),
				},

				Koute.Route "/xyz" {
					view = createView("at /xyz"),
				},
			}
		}
	}
}
```
:::