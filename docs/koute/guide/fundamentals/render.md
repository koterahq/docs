# Rendering the result
::: info Required code
```lua
local Players = game:GetService("Players")
local ReplicatedStorage = game:GetService("ReplicatedStorage")

local LocalPlayer = Players.LocalPlayer
local Packages = ReplicatedStorage.Packages

local Fusion = require(Packages.Fusion)
local Koute = require(Packages.Koute)

local function createView(text: string): () -> (TextLabel)
	return function()
		return Fusion.New "TextLabel" {
			Size = UDim2.fromOffset(200, 50),
			Text = text,
		}
	end
end

local appRouter = Koute.Router {
	[Fusion.Children] = {
		Koute.Route "/abc" {
			view = createView("at /abc"),
			[Fusion.Children] = {
				Koute.Route "/def" {
					view = createView("at /def"),
				},

				Koute.Route "/xyz" {
					view = createView("at /xyz"),
				},
			}
		}
	}
}
```
:::

In the previous guides of the fundamentals series, we explained the expected result of each code to you. However, there is no way for you to test out whether the code actually produces the expected result or not. In this section, you will be learning a new class called Canvas.

Canvas is a class that displays whatever is rendered by the router class. With the advantages brought by `Fusion.State`, changes to the currently-served page gets applied instantly (except for non-state parameters). Unlike traditional methods, canvas be created multiple times despite using the same router for the render source.

To use a canvas, you create it just like how do you create a router:
```lua
Koute.Canvas {

}
```

Next, you have to provide the render source, which is the reference to the router:
```lua
Koute.Canvas {
	source = appRouter,
}
```

Canvas has lifecycle functions: `preRender` and `postRender`. The `preRender` function gets called before the next page gets rendered and displayed on the canvas. Where the `postRender` function gets called after the next page is rendered and displayed. To use those lifecycle functions, you just have to assign the corresponding member to an actual function:

```lua
Koute.Canvas {
	source = appRouter,
	postRender = function()
		print("Hello, world!")
	end,
}
```

The returned value of the constructor function will be a `Frame` which displays the rendered page. To actually have it shown on the screen, have it be a descendant of a `LayerCollector` (e.g. `ScreenGui`, `SurfaceGui`):

```lua
Fusion.New "ScreenGui" {
	Parent = LocalPlayer.PlayerGui,
	[Fusion.Children] = {
		Koute.Canvas {
			source = appRouter,
			postRender = function()
				print("Hello, world!")
			end,
		},
	},
}
```

Now you should be able to see the rendered page from the router.

___
::: info Finished code
```lua
local Players = game:GetService("Players")
local ReplicatedStorage = game:GetService("ReplicatedStorage")

local LocalPlayer = Players.LocalPlayer
local Packages = ReplicatedStorage.Packages

local Fusion = require(Packages.Fusion)
local Koute = require(Packages.Koute)

local function createView(text: string): () -> (TextLabel)
	return function()
		return Fusion.New "TextLabel" {
			Size = UDim2.fromOffset(200, 50),
			Text = text,
		}
	end
end

local appRouter = Koute.Router {
	[Fusion.Children] = {
		Koute.Route "/abc" {
			view = createView("at /abc"),
			[Fusion.Children] = {
				Koute.Route "/def" {
					view = createView("at /def"),
				},

				Koute.Route "/xyz" {
					view = createView("at /xyz"),
				},
			}
		}
	}
}

Fusion.New "ScreenGui" {
	Parent = LocalPlayer.PlayerGui,
	[Fusion.Children] = {
		Koute.Canvas {
			source = appRouter,
			postRender = function()
				print("Hello, world!")
			end,
		},
	},
}
```
:::