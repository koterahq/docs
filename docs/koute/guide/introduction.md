# Introduction
## What is Koute?
Koute is a declarative routing library for use in Fusion. It implements the ability to navigate between different states of an UI without any hardcoding, an idea from SPA libraries. Developers can implement a multi-page UI with Koute for their project without making the code messy or any boilerplate code.

## Why use Koute?
Koute or any routing libraries are definitely not a necessity to implement page navigation for UIs. You can always implement similar effects by re-rendering the entire UI with content from another page, but this adds up a lot of boilerplate code especially when both pages have common elements (e.g. header, sidebar, footer, et. c.).

For UI having a tremendous amount of pages, this method will simply not work as it adds up pressure to maintainability and affects performance. Making a change of the common elements mean you have to apply the same change to all pages having those common elements.

Koute eliminates those disadvantages by raising the idea of UI routing in Roblox.