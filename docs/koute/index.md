---
title: Koute
titleTemplate: Routing in Fusion. Supercharged.
description: Koute is a lightweight routing library for Fusion. Open-sourced under the MIT license.
layout: home

hero:
  name: Koute
  text: Lightweight routing library for Fusion.
  tagline: Routing in Fusion. Supercharged.
  actions:
    - theme: brand
      text: Get Started
      link: koute/guide/introduction
    - theme: alt
      text: View on GitLab
      link: https://gitlab.com/koterahq/koute/prod/
features:
  - icon: 🪶
    title: Lightweight and simple
    details: Designed to be minimal. Koute does not affect your project's performance nor size by a lot.
  - icon: 👋
    title: Fusion meets routing
    details: Reactive states from Fusion makes routing extremely simple and fast. Barely anything new to learn.
---
