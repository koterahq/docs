---
aside: false
---
# Migration guides
## Migrating from `Koute@0.x`

Koute is now rewritten and released as `1.x`, some groundbreaking changes are included in this rewrite. For those who are using `0.x` in their project and plans to switch to `1.x` soon, here you can find a list of what is changed in `1.x`:

| Item          | Status        | Description  |
| ------------- |:-------------:| :-----|
| Behaviour options    | Removed | Can not find proper usage at the moment. |
| Object methods and members     | Changed | All object methods and members now use `camelCase` except for `Koute.Meta`, `Koute.Router`, `Koute.Route` and `Koute.Canvas`. |
| Meta     | Changed | Meta is now a symbol, no longer a dictionary table. |
| Dynamic      | Removed      | Dynamic meta function is no longer a thing in `1.x`. |
| Lazy | Removed      |  Lazy-loading was found to be useless and has been removed in `1.x`. |

## Migrating from `Fusion@0.1.x`

Starting with `Koute@1.0.2`, Koute will support for both `0.1.x` and `0.2` out of the box. No action is required.